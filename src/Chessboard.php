<?php

namespace ChessboardTestTask;

use ChessboardTestTask\Piece;
use ChessboardTestTask\Piece\AbstractPiece;
use ChessboardTestTask\Storage\IStorage;
use Closure;
use RangeException;
use RuntimeException;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Chessboard
{

    const CLASSICAL_SIZE = 8;
    const MIN_SIZE       = 1;
    const MAX_SIZE       = 26;

    /**
     * Размеры доски
     * @var int
     */
    private $size;
    /**
     * Клетки доски - занятые фигурами, пустые не храним, потому-что незачем
     * @var array
     */
    private $cellMap          = [];
    /**
     * Калбеки вызываемые при добавляении фигуры на доску
     * @var Closure[]
     */
    private $allocateCallback = [];

    /**
     *
     * @param int $size размер доски, по умолчанию 8
     * @throws RuntimeException
     */
    public function __construct($size = self::CLASSICAL_SIZE)
    {
        if ($size < self::MIN_SIZE) {
            throw new RuntimeException('Board too small');
        }
        if ($size > self::MAX_SIZE) {
            throw new RuntimeException('Board too large');
        }
        $this->size = $size;
    }

    /**
     * Размер доски
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Размещает фигуру на доске в клетке по названию клетки
     * @param string $cellName имя клетки в виде символЦифра
     * @param AbstractPiece $piece
     * @param boolean $forced - разместить даже если клетка занята другой фигурой
     * @throws RuntimeException - некоректный формат в $cellName
     */
    public function allocatePiece($cellName, Piece\AbstractPiece $piece, $forced = false)
    {
        $cell = new ChessboardCell($cellName);
        $cell->setPiece($piece);
        $this->pushCell($cell, $forced);

        if (isset($this->allocateCallback[Piece\AbstractPiece::class])) {
            $this->allocateCallback[Piece\AbstractPiece::class]($cell);
        }
        if (isset($this->allocateCallback[get_class($piece)])) {
            $this->allocateCallback[get_class($piece)]($cell);
        }
    }

    /**
     * Добавляет калбек на размещение фигуры определенного класса, либо на все если $pieceClass не указан
     * @param Closure $callback на размещение какой из фигур будут срабатывать калбек, null на любую
     * @param string $pieceClass
     */
    public function onAllocate(Closure $callback, $pieceClass = null)
    {
        if ($pieceClass === null) {
            $pieceClass = Piece\AbstractPiece::class;
        }

        $this->allocateCallback[$pieceClass] = $callback;
    }

    /**
     * Размещает фигуру на доске в указаной клетке
     * @param ChessboardCell $cell
     * @param boolean $forced - разместить даже если клетка занята другой фигурой
     * @throws RuntimeException - клетка уже занята
     * @throws RangeException - координаты клетки за пределами доски
     */
    protected function pushCell(ChessboardCell $cell, $forced = false)
    {
        if (!$forced && !$this->isEmptyCell($cell->getName())) {
            throw new RuntimeException('Distanation cell is not empty');
        }

        if ($cell->getX() > $this->size || $cell->getY() > $this->size) {
            throw new RangeException('Cell position out of range');
        }

        $this->cellMap[$cell->getName()] = $cell;
    }

    /**
     * Возвращает фигуру расположеную в указанной клетке
     * @param string $cellName имя клетки в виде символЦифра
     * @return Piece\AbstractPiece
     * @throws RuntimeException в указной клетке нет фигуры
     */
    public function getPiece($cellName)
    {
        if ($this->isEmptyCell($cellName)) {
            throw new RuntimeException('Piece not exist');
        }
        return $this->cellMap[strtolower($cellName)]->getPiece();
    }

    /**
     * Убирает фигуру с указанной клетки
     * @param string $fromCellName имя клетки в виде символЦифра
     */
    public function removePiece($fromCellName)
    {
        unset($this->cellMap[$fromCellName]);
    }

    /**
     *
     * @param string $fromCellName имя клетки в виде символЦифра ОТКУДА перемещаем фигуру
     * @param string $toCellName имя клетки в виде символЦифра КУДА перемещаем фигуру
     * @param boolean $forced если true, то перемещаем даже если целевая клетка занята другой фигурой
     */
    public function relocatePiece($fromCellName, $toCellName, $forced = false)
    {
        $targetCell = new ChessboardCell($toCellName);
        $targetCell->setPiece($this->getPiece($fromCellName));
        $this->pushCell($targetCell, $forced);
        $this->removePiece($fromCellName);
    }

    /**
     * Проверяет клетку на незанятость
     * @param string $cellName имя клетки в виде символЦифра
     * @return boolean true - клетка свободна
     */
    public function isEmptyCell($cellName)
    {
        $cellName = strtolower($cellName);
        return empty($this->cellMap[$cellName]) || $this->cellMap[$cellName]->getPiece() === null;
    }

    /**
     * Записывает состояние доски в хранилище
     * @throws Storage\StorageException в случае ошибки при записи в хранилище
     * @param AbstractStorage
     */
    function saveToStorage(IStorage $storage)
    {
        $data = [
            'size' => $this->size,
            'cell' => serialize($this->cellMap),
        ];
        $storage->save(json_encode($data));
    }

    /**
     * Загружает состояние доски из хранилища
     * @throws Storage\StorageException в случае ошибки при чтении из хранилища
     * @param AbstractStorage
     */
    function loadFromStorage(IStorage $storage)
    {
        $data = json_decode($storage->load(), true);
        if (!isset($data['size'], $data['cell'])) {
            throw new RuntimeException('Incorrect serialized data');
        }
        $this->size    = $data['size'];
        $this->cellMap = unserialize($data['cell']);
    }
}
