<?php

namespace ChessboardTestTask;

use ChessboardTestTask\Piece\AbstractPiece;
use RuntimeException;
use Serializable;

/**
 * Клетка доски
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ChessboardCell implements \Serializable
{

    /**
     * Имя клетки в виде символНомер
     * @var string
     */
    private $name;
    /**
     *
     * @var int
     */
    private $x;
    /**
     *
     * @var int
     */
    private $y;
    /**
     * @var AbstractPiece
     */
    private $piece;

    /**
     *
     * @param string $name имя клетки в виде символНомер
     */
    public function __construct($name)
    {
        $this->parseName($name);
    }

    /**
     * Разбирает и валидирует имя клетки, в случае успеха запоминает координаты в свойствах клетки x и y
     * @param string $name имя клетки в виде символНомер
     * @throws RuntimeException
     */
    private function parseName($name)
    {
        $matches    = [];
        $this->name = strtolower($name);
        if (!preg_match('/^([a-z])([0-9]{1,2})$/i', $this->name, $matches)) {
            throw new RuntimeException('Incorrect cell name `' . $this->name . '`');
        }
        $this->x = ord($matches[1]) - ord('a') + 1;
        $this->y = (int) $matches[2];
        if ($this->y < 1) {
            throw new RuntimeException('Incorrect cell name');
        }
    }

    /**
     * Имя клетки в виде символНомер
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Положение клетки на доске по горизонтали 1-26 (для a-z)
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Положение клетки на доске по вертикали
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Добавляет фигуру в клетку
     * @param AbstractPiece
     */
    function setPiece(AbstractPiece $piece)
    {
        $this->piece = $piece;
    }

    /**
     * Фигура размещеная в клетке, либо null
     * @return AbstractPiece|null
     */
    public function getPiece()
    {
        return $this->piece;
    }

    /**
     *
     * @return boolean true - если в клетке размещена фигура
     */
    public function hasPiece()
    {
        return $this->piece !== null;
    }

    /**
     * Освобождает клетку от фигуры
     */
    public function removePiece()
    {
        $this->piece = null;
    }

    /**
     * признак того что клетка белого цвета
     * @return boolean true - белая клетка, false - черная клетка
     */
    public function isWhite()
    {
        return $this->x % 2 !== $this->y % 2;
    }

    public function serialize()
    {
        $data = [
            'name'  => $this->name,
            'piece' => serialize($this->piece),
        ];
        return json_encode($data);
    }

    public function unserialize($serialized)
    {
        $data = json_decode($serialized, true);
        if (!isset($data['name'], $data['piece'])) {
            throw new RuntimeException('Incorrect serialized data');
        }

        $this->parseName($data['name']);
        $this->setPiece(unserialize($data['piece']));
    }
}
