<?php

namespace ChessboardTestTask\Piece;

/**
 * Фигура - ладья
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Rook extends AbstractPiece
{

    const NAME = 'rook';

    /**
     * Наименоваени фигуры
     */
    public function getName()
    {
        return self::NAME;
    }
}
