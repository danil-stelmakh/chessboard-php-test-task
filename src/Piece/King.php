<?php

namespace ChessboardTestTask\Piece;

/**
 * Фигура - король
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class King extends AbstractPiece
{

    const NAME = 'king';

    /**
     * Наименоваени фигуры
     */
    public function getName()
    {
        return self::NAME;
    }
}
