<?php

namespace ChessboardTestTask\Piece;

/**
 * Фигура - конь
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Knight extends AbstractPiece
{

    const NAME = 'knight';

    /**
     * Наименоваени фигуры
     */
    public function getName()
    {
        return self::NAME;
    }
}
