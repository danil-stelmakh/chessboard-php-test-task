<?php

namespace ChessboardTestTask\Piece;

/**
 * Фигура - ферзь
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Queen extends AbstractPiece
{

    const NAME = 'queen';

    /**
     * Наименоваени фигуры
     */
    public function getName()
    {
        return self::NAME;
    }
}
