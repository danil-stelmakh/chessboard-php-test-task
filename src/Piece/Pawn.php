<?php

namespace ChessboardTestTask\Piece;

/**
 * Фигура - пешка
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Pawn extends AbstractPiece
{

    const NAME = 'pawn';

    /**
     * Наименоваени фигуры
     */
    public function getName()
    {
        return self::NAME;
    }
}
