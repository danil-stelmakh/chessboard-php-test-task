<?php

namespace ChessboardTestTask\Piece;

use Serializable;

/**
 * Базовый класс для всех фигур
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
abstract class AbstractPiece implements Serializable
{

    /**
     * Признак белого цвета фигуры
     * @var boolean true - белая, false - черная
     */
    private $isWhite;

    /**
     * 
     * @param  boolean true - белая, false - черная
     */
    public function __construct($isWhite)
    {
        $this->isWhite = (boolean) $isWhite;
    }

    /**
     * Признак белого цвета фигуры
     * @return boolean true - белая, false - черная
     */
    public function isWhite()
    {
        return $this->isWhite;
    }

    /**
     * @return string Наименование фигуры
     */
    abstract public function getName();

    /**
     * Возвращает представление объекта в виде массива
     * @return array
     */
    protected function getAsArray()
    {
        return [
            'isWhite' => $this->isWhite(),
        ];
    }

    /**
     * Заполянет свойства объекта по значениям из массива
     * @return array
     */
    public function setFromArray($data)
    {
        if (!isset($data['isWhite'])) {
            throw new RuntimeException('Incorrect serialized data');
        }
        $this->isWhite = (boolean) $data['isWhite'];
    }

    public function serialize()
    {
        return json_encode($this->getAsArray());
    }

    public function unserialize($serialized)
    {
        $this->setFromArray(json_decode($serialized, true));
    }
}
