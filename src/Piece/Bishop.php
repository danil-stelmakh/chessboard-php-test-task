<?php

namespace ChessboardTestTask\Piece;

/**
 * Фигура - слон
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Bishop extends AbstractPiece
{

    const NAME = 'bishop';

    /**
     * Наименоваени фигуры
     */
    public function getName()
    {
        return self::NAME;
    }
}
