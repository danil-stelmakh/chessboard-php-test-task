<?php

namespace ChessboardTestTask\Storage;

/**
 * Интерфейс для хранилища доски
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
interface IStorage
{

    /**
     * Записывает строку в хранилище
     * @param string $string
     */
    public function save($string);

    /**
     * Читает строку из хранилище
     * @return string
     */
    public function load();
}
