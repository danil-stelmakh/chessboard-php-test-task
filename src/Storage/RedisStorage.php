<?php

namespace ChessboardTestTask\Storage;

use Exception;
use Redis;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class RedisStorage implements IStorage
{

    const KEY = 'chess_board';

    /**
     *
     * @var Redis
     */
    private $redis;

    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    public function load()
    {
        try {
            return $this->redis->get(self::KEY);
        } catch (Exception $ex) {
            throw new StorageException($ex->getMessage());
        }
    }

    public function save($string)
    {
        try {
            return $this->redis->set(self::KEY, addslashes($string));
        } catch (Exception $ex) {
            throw new StorageException($ex->getMessage());
        }
    }
}
