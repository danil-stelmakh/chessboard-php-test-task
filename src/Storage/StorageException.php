<?php

namespace ChessboardTestTask\Storage;

use RuntimeException;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class StorageException extends RuntimeException
{
    //put your code here
}
