<?php

namespace ChessboardTestTask\Storage;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class FileStorage implements IStorage
{
    private $fileName;


    public function __construct($fileName)
    {
        $this->fileName = (string) $fileName;
    }

    public function load()
    {
        if(!file_exists($this->fileName)) {
            throw new StorageException("File `{$this->fileName}` not exist");
        }
        file_get_contents($this->fileName);
    }

    public function save($string)
    {
        if(!is_writable($this->fileName)) {
            throw new StorageException("File `{$this->fileName}` not writable");
        }
        file_put_contents($this->fileName, $string);
    }
}
