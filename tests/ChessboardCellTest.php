<?php

use ChessboardTestTask\ChessboardCell;
use ChessboardTestTask\Piece\Pawn;
use ChessboardTestTask\Piece\Queen;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ChessboardCellTest extends PHPUnit_Framework_TestCase
{

    public function testCrate()
    {
        new ChessboardCell('a1');

        try {
            new ChessboardCell('');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            new ChessboardCell('1');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            new ChessboardCell('11');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            new ChessboardCell('a');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            new ChessboardCell('ab');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            new ChessboardCell('aa10');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            new ChessboardCell('a100');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }
    }

    public function testPositionAndColor()
    {
        for($x = 1; $x<=26; $x++) {
            for($y = 1; $y<=26; $y++) {
                $cell = new ChessboardCell(chr($x + 96) . $y);
                $this->assertEquals($x, $cell->getX());
                $this->assertEquals($y, $cell->getY());
                $this->assertEquals(($x % 2 !== $y % 2), $cell->isWhite());

            }
        }
    }

    public function testSetPiece()
    {
        $queen = new Queen(true);
        $pawn  = new Pawn(true);

        $cell = new ChessboardCell('a1');

        $this->assertNull($cell->getPiece());

        $cell->setPiece($queen);
        $this->assertEquals($queen, $cell->getPiece());

        $cell->setPiece($pawn);
        $this->assertEquals($pawn, $cell->getPiece());
    }

    public function testRemovePiece()
    {
        $queen = new Queen(true);

        $cell = new ChessboardCell('a1');

        $this->assertNull($cell->getPiece());

        $cell->setPiece($queen);
        $this->assertEquals($queen, $cell->getPiece());

        $cell->removePiece();
        $this->assertNull($cell->getPiece());
    }
}
