<?php

use ChessboardTestTask\Chessboard;
use ChessboardTestTask\ChessboardCell;
use ChessboardTestTask\Piece\Pawn;
use ChessboardTestTask\Piece\Queen;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ChessboardTest extends PHPUnit_Framework_TestCase
{

    public function testCreate()
    {
        $chessboard = new Chessboard();
        $this->assertEquals(Chessboard::CLASSICAL_SIZE, $chessboard->getSize());

        $chessboard = new Chessboard(12);
        $this->assertEquals(12, $chessboard->getSize());

        try {
            new Chessboard(Chessboard::MIN_SIZE - 1);
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            new Chessboard(Chessboard::MAX_SIZE + 1);
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }
    }

    public function testAllocatePiece()
    {
        $chessboard = new Chessboard();
        $queen      = new Queen(true);
        $pawn       = new Pawn(true);


        $this->assertTrue($chessboard->isEmptyCell('a1'));
        $chessboard->allocatePiece('a1', $queen);
        $this->assertEquals($queen, $chessboard->getPiece('a1'));
        $this->assertFalse($chessboard->isEmptyCell('a1'));

        try {
            $chessboard->allocatePiece('a9', $queen);
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            $chessboard->allocatePiece('i1', $queen);
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        try {
            $chessboard->allocatePiece('a1', $pawn);
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        $chessboard->allocatePiece('a1', $pawn, true);
        $this->assertEquals($pawn, $chessboard->getPiece('a1'));
    }

    public function testRemovePiece()
    {
        $chessboard = new Chessboard();
        $queen      = new Queen(true);
        $pawn       = new Pawn(false);

        $this->assertTrue($chessboard->isEmptyCell('a1'));
        $this->assertTrue($chessboard->isEmptyCell('b2'));

        $chessboard->allocatePiece('a1', $queen);
        $chessboard->allocatePiece('b2', $pawn);

        $this->assertFalse($chessboard->isEmptyCell('a1'));
        $this->assertFalse($chessboard->isEmptyCell('b2'));

        $chessboard->removePiece('a1', $queen);
        $this->assertTrue($chessboard->isEmptyCell('a1'));
        $this->assertFalse($chessboard->isEmptyCell('b2'));

        $chessboard->removePiece('b2', $queen);
        $this->assertTrue($chessboard->isEmptyCell('a1'));
        $this->assertTrue($chessboard->isEmptyCell('b2'));
    }

    public function testRelocatePiece()
    {
        $chessboard = new Chessboard();
        $queen      = new Queen(true);
        $pawn       = new Pawn(false);

        $this->assertTrue($chessboard->isEmptyCell('a1'));
        $this->assertTrue($chessboard->isEmptyCell('b2'));

        $chessboard->allocatePiece('a1', $queen);
        $chessboard->allocatePiece('b2', $pawn);

        $this->assertEquals($queen, $chessboard->getPiece('a1'));
        $this->assertEquals($pawn, $chessboard->getPiece('b2'));

        try {
            $chessboard->relocatePiece('a1', 'b2');
            $this->fail('Missing expected RuntimeException');
        } catch (RuntimeException $ex) {
            $this->addToAssertionCount(1);
        }

        $this->assertEquals($queen, $chessboard->getPiece('a1'));
        $this->assertEquals($pawn, $chessboard->getPiece('b2'));

        $chessboard->relocatePiece('a1', 'b1');

        $this->assertTrue($chessboard->isEmptyCell('a1'));
        $this->assertEquals($queen, $chessboard->getPiece('b1'));
        $this->assertEquals($pawn, $chessboard->getPiece('b2'));

        $chessboard->relocatePiece('b1', 'b2', true);
        $this->assertTrue($chessboard->isEmptyCell('b1'));
        $this->assertEquals($queen, $chessboard->getPiece('b2'));
    }

    public function testAllocatePieceCallback()
    {
        $chessboard = new Chessboard();
        $queen      = new Queen(true);
        $pawn       = new Pawn(false);

        $messages = [];

        $globalCallback = function(ChessboardCell $cell) use (&$messages) {
            $messages[] = 'global:' . $cell->getName() . ':' . $cell->getPiece()->getName();
        };

        $queenCallback = function(ChessboardCell $cell) use (&$messages) {
            $messages[] = 'queen:' . $cell->getName() . ':' . $cell->getPiece()->getName();
        };


        $chessboard->onAllocate($globalCallback);
        $chessboard->onAllocate($queenCallback, Queen::class);

        $chessboard->allocatePiece('b1', $pawn);
        $chessboard->allocatePiece('b2', $pawn);
        $chessboard->allocatePiece('a1', $queen);
        $chessboard->allocatePiece('b3', $pawn);
        $chessboard->allocatePiece('a2', $queen);

        $expect = [
            'global:b1:pawn',
            'global:b2:pawn',
            'global:a1:queen',
            'queen:a1:queen',
            'global:b3:pawn',
            'global:a2:queen',
            'queen:a2:queen',
        ];

        $this->assertEquals($expect, $messages);
    }

    public function testSaveAndLoad()
    {
        $chessboard = new Chessboard();
        $queen      = new Queen(true);
        $pawn       = new Pawn(false);

        $this->assertTrue($chessboard->isEmptyCell('a1'));
        $this->assertTrue($chessboard->isEmptyCell('b2'));

        $chessboard->allocatePiece('a1', $queen);
        $chessboard->allocatePiece('b2', $pawn);

        $this->assertEquals($queen, $chessboard->getPiece('a1'));
        $this->assertEquals($pawn, $chessboard->getPiece('b2'));

        $storageMockBuilder = $this->getMockBuilder(\ChessboardTestTask\Storage\FileStorage::class);
        $storage            = $storageMockBuilder->setMethods(['save', 'load'])
                ->setConstructorArgs([''])
                ->getMock();

        $content = '';
        $storage->expects($this->once())
                ->method('save')->will(
                $this->returnCallback(function($string) use (&$content) {
                    $content = $string;
                }));
        $chessboard->saveToStorage($storage);

        $this->assertJson($content);

        $newChessboard = new Chessboard(1);

        $storage->expects($this->once())
                ->method('load')
                ->willReturn($content);

        $newChessboard->loadFromStorage($storage);

        $this->assertEquals($chessboard, $newChessboard);

        $this->assertEquals($queen, $newChessboard->getPiece('a1'));
        $this->assertEquals($pawn, $newChessboard->getPiece('b2'));
    }
}
